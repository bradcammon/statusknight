<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();
        
//        $schedule->call(function () {
//            $user = ['name' => 'Hippy', 'id' => 1];
//            \Mail::send('emails.updateItems',['user'=> $user], function ($m){
//                $m->to('hipnoddic@mac.com', 'Hippy')->subject('Your Reminder');
//            });
//        })->yearly();

        $schedule->call(function () {
            //TODO: remove hardcoding
            //TODO: setup queuing
            $utc_date = \Carbon\Carbon::create(
                \Config::get('constants.base_date.year'),
                \Config::get('constants.base_date.month'),
                10,
                01, 0, 0,
                'UTC');
            $utc_time = $utc_date->toTimeString();
            $today = strtolower($utc_date->format('l'));

            $users = \DB::table('users')
                ->join('schedules', 'users.id', '=', 'schedules.user_id')
                ->where('schedules.utc_time', '=', $utc_time)
                ->where('schedules.'.$today, '=', 1)
                ->select('users.email', 'users.id', 'users.name')
                ->get();

            foreach ($users as $user ) {
                \Mail::send('emails.updateItems',['user'=> $user], function ($m) use ($user){
                    $m->to($user->email, $user->name)->subject('Your Reminder');
                });
            }

        })->daily();
    }
}
