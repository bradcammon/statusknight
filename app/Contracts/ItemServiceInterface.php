<?php

namespace App\Contracts;


interface ItemServiceInterface
{
    public function getItemsByDate( $date );
    public function storeItem( $request );
    public function destroyItem( $id );
    public function updateItem( $request, $id );

}