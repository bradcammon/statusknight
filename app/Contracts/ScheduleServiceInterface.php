<?php

namespace App\Contracts;


interface ScheduleServiceInterface
{
    public function updateSchedule( $request, $id );
    public function storeTimezone( $timezoneString );
    public function showSchedule();

}