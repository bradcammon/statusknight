<?php

namespace App\Contracts;


interface UserServiceInterface
{
    public function updateUser( $request, $id );
    public function updatePassword( $request, $id );
    public function showUserSettings();
    public function updateTeam( $request, $id );
    public function createUserAndRelatedModels( array $data );
    public function getReport( $start_date, $end_date );

}