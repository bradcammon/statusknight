<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\FatalErrorException;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

use Log;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
        ModelNotFoundException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {

        if ($e instanceof Exception) {
            Log::error($e->__toString());
        }

        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        // For all HTTP status codes, simply placing a file named '404.blade.php',
        // for example, in views/errors, will take care of global handling for
        // those types of exceptions.  Nothing needed here.

        if ($e instanceof ModelNotFoundException) {
            $e = new NotFoundHttpException($e->getMessage(), $e);
        }

        if ($e instanceof NotFoundHttpException) {
            return response()->view('errors.404', [], 404);
        }

        if ($e instanceof MethodNotAllowedHttpException) {
            //$error_title = $e->getMessage();
            return response()->view('errors.500', ['error_title' => 'Method Not Allowed'], 500);
        }

//        if ($e instanceof FatalErrorException) {
//            return response()->view('errors.500', ['error_title' => 'Fatal Error'], 500);
//        }

        if ($e instanceof Exception) {
            $error_title = '';
            if ($e->getMessage())
            {
                $error_title = $e->getMessage();
            }
            return response()->view('errors.500', ['error_title' => $error_title], 500);
        }

        return parent::render($request, $e);
    }
}
