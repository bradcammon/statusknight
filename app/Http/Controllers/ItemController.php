<?php

namespace App\Http\Controllers;

use App\Exceptions\ItemDeleteException;
use Illuminate\Http\Request;
use App\Http\Requests;
use Exception;
use Log;
use Lang;
use Auth;
use App\Http\Requests\StoreItemPostRequest;
use App\Http\Requests\EditItemRequest;
use App\Contracts\ItemServiceInterface;
use App\Http\Requests\ListItemsRequest;

/**
 * Class ItemController
 * @package App\Http\Controllers
 */
class ItemController extends Controller
{
    protected $item;

    /**
     * Create a new controller instance, and inject the service which
     * handles the related business logic.
     */
    public function __construct( ItemServiceInterface $item )
    {
         // Use middleware to ensure the requestor is authenticated in order
         // to access this controller.       
//        $this->middleware('auth');
        $this->middleware('auth', ['except' => ['create']]);
        $this->item = $item;
    }

    /**
     * Show the main items page where a user can manage status items.
     * Front-end javascript/ajax handles querying the datastore.
     *
     * @param Request $request
     * @return \Response
     */
    public function create( Request $request)
    {
        $user = Auth::user();

        if (!$user)
        {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return view('pages.landing');
            }
        }
        $team = $user->ownedTeams()->first();

        return view('items.home', ['teamName' => $team->name]);
    }


    /**
     * Returns a list of all status items for the requesting user.
     *
     * @param ListItemsRequest|Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index( ListItemsRequest $request )
    {
        try
        {
            return $this->item->getItemsByDate( $request->input('date') );
        }
        catch (Exception $e)
        {
            return $this->handleGenericException($e);
        }
    }



    /**
     * Stores a new status item for the user.
     *
     * @param Requests\StoreItemPostRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store( StoreItemPostRequest $request )
    {
        try
        {
            return $this->item->storeItem( $request );
        }
        catch (Exception $e)
        {
            return $this->handleGenericException($e);
        }
    }




    /**
     * Deletes a specific status item.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try
        {
            return $this->item->destroyItem( $id );
        }
        catch (ItemDeleteException $e)
        {
             return response()->json(['success' => "false", 'message' => Lang::get('itemResponse.delete_exception')]);
        }
        catch (Exception $e)
        {
            return $this->handleGenericException($e);
        }
    }

    /**
     * Updates a specific status item.
     *
     * @param Requests\EditItemRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(EditItemRequest $request, $id)
    {
        try
        {
            return $this->item->updateItem( $request, $id );
        }
        catch (Exception $e)
        {
            return $this->handleGenericException($e);
        }
    }

    /**
     * Handle generic exceptions by logging them and returning a non-specific error message.
     *
     * @param $e
     * @return \Illuminate\Http\JsonResponse
     */
    private function handleGenericException($e)
    {
        Log::error('Line: '.$e->__toString());
        // from http://stackoverflow.com/a/674142
        return response()->json(['success' => 'false', 'message' => Lang::get('itemResponse.general_error')]);
    }

}
