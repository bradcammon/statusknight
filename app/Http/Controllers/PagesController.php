<?php

namespace App\Http\Controllers;

use App\Contracts\RocketShipContract;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    
    public function __construct()
    {
            $this->middleware('auth', ['except' => 'welcome']);
            $this->middleware('guest', ['only' => 'welcome']);
    }    
    
    public function home()
    {
        return view('pages.home');
    }
    
    public function welcome()
    {
//        return view('pages.guest');
        return view('pages.landing');
    }
}
