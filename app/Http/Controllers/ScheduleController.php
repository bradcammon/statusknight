<?php

namespace App\Http\Controllers;

use App\Contracts\ScheduleServiceInterface;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserUpdateScheduleRequest;
use App\Exceptions\ApplicationErrorException;
use Exception;
use Log;


class ScheduleController extends Controller
{
    protected $schedule;

    /**
     * Create a new controller instance, and inject the service which
     * handles the related business logic.
     */
    public function __construct( ScheduleServiceInterface $schedule )
    {
        // Use middleware to ensure the requestor is authenticated in order
        // to access this controller.
        $this->middleware('auth');
        $this->schedule = $schedule;
    }

    public function showSchedule()
    {
        try
        {
            $schedule = $this->schedule->showSchedule();
            return view('users.schedule', $schedule );
        }
        catch (ApplicationErrorException $e)
        {
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Update a the reminder email schedule.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|string
     */
    public function scheduleUpdate( UserUpdateScheduleRequest $request, $id )
    {
        try
        {

            $this->schedule->updateSchedule( $request, $id );

            return redirect('schedule');
        }
        catch (ApplicationErrorException $e)
        {
            Log::error($e->__toString());
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    // TODO: validate
    /**
     * Store the browser-detected timezone.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function storeTimezone( Request $request )
    {
        try
        {
            $this->schedule->storeTimezone( $request->timezone );
            return response()->json(['success' => "true", 'message' => '', 'timezone' => $request->timezone]);
        }
        catch (Exception $e)
        {
            Log::error($e->__toString());
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }
    }





}
