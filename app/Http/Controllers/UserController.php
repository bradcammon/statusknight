<?php

namespace App\Http\Controllers;

use App\Contracts\UserServiceInterface;
use App\Exceptions\ApplicationErrorException;
use App\Http\Requests\UserUpdateTeamRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use Exception;
use Log;
use Lang;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UserUpdatePasswordRequest;
use App\Http\Requests\UserUpdateScheduleRequest;

class UserController extends Controller
{
    protected $user;

    public function __construct( UserServiceInterface $user )
    {
        // Use middleware to ensure the requestor is authenticated in order
        // to access this controller.
        $this->middleware('auth');
        $this->user = $user;
    }


    /**
     * Update the specified user info in the datastore.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update( UpdateUserRequest $request, $id )
    {
        try
        {
            $this->user->updateUser( $request, $id );

            session()->flash('flash_message_success', 'Your profile has been updated.');

            return redirect('user/settings');
        }
        catch (ApplicationErrorException $e)
        {
            Log::error($e->__toString());
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Update the user's password.
     *
     * @param UserUpdatePasswordRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|string
     */
    public function updatePassword( UserUpdatePasswordRequest $request, $id )
    {
        try
        {
            $response = $this->user->updatePassword( $request, $id );


            session()->flash('flash_message_success', 'Your password has been updated.');

            return redirect('user/settings')
                ->withErrors($response, 'password');

        }
        catch (ApplicationErrorException $e)
        {
            Log::error($e->__toString());
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Update the user's owned team
     *
     * @param UserUpdateTeamRequest|Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function teamUpdate( UserUpdateTeamRequest $request, $id )
    {
        try
        {
            $this->user->updateTeam( $request, $id );
            return redirect('user/settings');
        }
        catch (ApplicationErrorException $e)
        {
            Log::error($e->__toString());
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }
    }


    /**
     * Display the page to update a user's settings
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showSettings()
    {
        try
        {
            $settings = $this->user->showUserSettings();
            return view('users.settings', $settings );
        }
        catch (ApplicationErrorException $e)
        {
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }
    }


    /**
     *
     */
    public function getReport( Request $request )
    {
        //http://statusknight.dev:8888/user/report?startDate=2015-12-14&endDate=2015-12-18

        try
        {
            $data = $this->user->getReport( $request->startDate, $request->endDate) ;
//            dd($data);

            return view('users.report', $data);
        }
        catch (ApplicationErrorException $e)
        {
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    private function handleGenericException($e)
    {
        Log::error($e->__toString());
        return Lang::get('itemResponse.general_error');
    }

}
