<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreItemPostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // Our middleware should handle the authorization.
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item'  => 'required|min:3|max:255',
            'status_date' => 'required|date'
        ];
    }
}
