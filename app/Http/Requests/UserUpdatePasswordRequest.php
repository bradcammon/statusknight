<?php

namespace App\Http\Requests;

use Auth;

class UserUpdatePasswordRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // Our middleware should handle the authorization.
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();

        return [
            'old_password'  => 'required',
            'new_password'  => 'required|min:6|max:32|confirmed',
        ];
    }
}
