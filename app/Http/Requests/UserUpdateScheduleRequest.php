<?php

namespace App\Http\Requests;

use Auth;

class UserUpdateScheduleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // Our middleware should handle the authorization.
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email_time'  => 'required|numeric|min:1|max:24',
            'timezone'  => 'required|numeric',
        ];
    }
}
