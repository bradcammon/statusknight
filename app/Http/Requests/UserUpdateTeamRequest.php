<?php

namespace App\Http\Requests;

use Auth;

class UserUpdateTeamRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // Our middleware should handle the authorization.
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();

        return [
            'name'  => 'required|min:3|max:255',
        ];
    }
}
