<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('home', 'PagesController@home');
//Route::get('/', 'PagesController@home');
Route::get('welcome', 'PagesController@welcome');

// Item routes
Route::get('item', 'ItemController@index');
//Route::get('item/create', 'ItemController@create');
Route::get('home', 'ItemController@create');

Route::get('/', 'ItemController@create');
Route::get('item/email/{id}', 'ItemController@sendEmailReminder' );
Route::post('item', 'ItemController@store');
Route::delete('item/{item}', 'ItemController@destroy');
Route::put('item/{item}', 'ItemController@update');


// User routes
Route::get('user/settings', 'UserController@showSettings');
Route::get('user/{user}/edit', 'UserController@edit');
Route::put('user/{user}/password', 'UserController@updatePassword');
Route::put('user/{user}', 'UserController@update');
Route::put('team/{user}', 'UserController@teamUpdate');
Route::get('user/query', 'UserController@emailQuery');

// Schedule routes
Route::put('schedule/{user}', 'ScheduleController@scheduleUpdate');
Route::post('schedule/timezone', 'ScheduleController@storeTimezone');
Route::get('schedule', 'ScheduleController@showSchedule');

// Report routes
Route::get('user/report', 'UserController@getReport');

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');