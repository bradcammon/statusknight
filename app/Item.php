<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    /**
     * Fillable fields for a new item.
     * 
     * @var array 
     */
    protected $fillable = [
        'raw_text',
        'status_date'
    ];
}
