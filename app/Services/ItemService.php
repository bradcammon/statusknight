<?php

namespace App\Services;


use App\Contracts\ItemServiceInterface;
use App\Exceptions\ItemDeleteException;
use App\Item;
use Auth;
use Lang;
use Exception;
use Log;

class ItemService implements ItemServiceInterface
{
    public function getItemsByDate( $date )
    {
      $items = Auth::user()->items()->where('status_date',$date)->oldest()->get();
      return $items;
    }

    public function storeItem( $request )
    {
        $item = new Item();
        $item->raw_text = $request->item;
        $item->status_date = $request->status_date;
        $result = Auth::user()->items()->save($item);

        // Created a nested json array, so the iterating function on the client-side
        // can treat single items the same as multi-item results
        // TODO: modify this response structure to follow the pattern below (success=true, etc)
        // ....this would also require a change to the front-end javascript
        return response()->json( array( $result ) );

    }

    public function destroyItem( $id )
    {
        $item = Auth::user()->items()->findOrFail($id);
        $success = $item->delete();

        if (!$success) {
            throw new ItemDeleteException();
        }
        return response()->json(['success' => "true", 'message' => Lang::get('itemResponse.item_deleted')]);

    }

    public function updateItem( $request, $id )
    {
        $item = Auth::user()->items()->findOrFail($id);
        $item->raw_text = $request->item;
        $item->save();
        return response()->json(['success' => 'true', 'item' => $item->raw_text]);
    }

}