<?php

namespace App\Services;


use App\Contracts\ScheduleServiceInterface;
use App\Exceptions\ApplicationErrorException;
use Auth;
use DateTimeZone;
use Carbon\Carbon;
use Config;


class ScheduleService implements ScheduleServiceInterface
{
    // TODO: Refactor this class to be more DRY...

    public function showSchedule()
    {
        $user = Auth::user();

        // A user can only have one team currently (relationship defined in the User model)
        //, so the result of the following query should be one record.
        //TODO: Make this team-centric (i.e. user could have n teams)

        if (! $team = $user->ownedTeams()->first() )
        {
            throw new ApplicationErrorException("No team found for user: $user->id");
        }

        if (! $schedule = $user->schedule()->first() )
        {
            throw new ApplicationErrorException("No schedule found for user: $user->id");
        }

        $localSchedule = $this->convertUTCToLocalSchedule( $schedule->timezone, $schedule->utc_time, $schedule );

        $localWeekdays = $localSchedule['localWeekdays'];

        $schedule->sunday = $localWeekdays['sunday'];
        $schedule->monday = $localWeekdays['monday'];
        $schedule->tuesday = $localWeekdays['tuesday'];
        $schedule->wednesday = $localWeekdays['wednesday'];
        $schedule->thursday = $localWeekdays['thursday'];
        $schedule->friday = $localWeekdays['friday'];
        $schedule->saturday = $localWeekdays['saturday'];

        // Process report schedule
        $reportDayObject = $this->createReportDayObject($schedule->report_day);

        $localReportSchedule = $this->convertUTCToLocalSchedule($schedule->timezone, $schedule->report_utc_time, $reportDayObject);

        $localReportWeekdays = $localReportSchedule['localWeekdays'];

        $userReportHoursKey = $localReportSchedule['localHour'];

        // Get timezone info
        $userTimeZoneKey = $this->convertTimezone($schedule->timezone);

        $tzList = DateTimeZone::listIdentifiers(DateTimeZone::ALL);

        $userHoursKey = $localSchedule['localHour'];
        if (! $userHoursKey)
        {
            throw new ApplicationErrorException("Time of day (hours key) not found for user: $user->id");
        }

        // Get array to display select box of hours
        $hours = $this->getHoursSelectArray();

        return compact(
            'localWeekdays',
            'localReportWeekdays',
            'user',
            'userTimeZoneKey',
            'tzList',
            'hours',
            'userHoursKey',
            'userReportHoursKey'
        );

    }

    public function storeTimezone( $timezoneString )
    {

        $userTimeZoneKey = $this->convertTimezone($timezoneString);

        // Get user schedule from DB, and convert it to local timezone
        $user = Auth::user();
        $schedule = $user->schedule()->first();

        $reminderDaysObject = $this->createReminderDaysObject($schedule);

        $localSchedule = $this->convertUTCToLocalSchedule($timezoneString, $schedule->utc_time, $reminderDaysObject);

        $localWeekdays = $localSchedule['localWeekdays'];

        $modifiedLocalWeekdays = $this->createReminderDaysArray( $localWeekdays );

        // Process report schedule
        $reportDayObject = $this->createReportDayObject($schedule->report_day);

        $localReportSchedule = $this->convertUTCToLocalSchedule($timezoneString, $schedule->report_utc_time, $reportDayObject);

        $localReportDay = array_search( 1, $localReportSchedule['localWeekdays'] );

        $userReportHoursKey = $localReportSchedule['localHour'];


        $constructedRequest = new \stdClass();

        $constructedRequest->reminder_days = $modifiedLocalWeekdays;
        $constructedRequest->email_time = $localSchedule['localHour'];
        $constructedRequest->timezone = $userTimeZoneKey;
        $constructedRequest->report_time = $userReportHoursKey;
        $constructedRequest->report_day = [
            0   =>  $localReportDay
        ];

        // TODO: Should we get a return code here?
        $this->updateSchedule( $constructedRequest, $user->id );

        // Assuming the schedule update was successful, update the user timezone flag,
        // so we don't poll for the local timezone next page load.
        $user->timezone_flag = 0;
        $user->save();
    }


    /**
     * Update the user's schedule, given a request object that includes
     * a timezone, time of day, and optional days of the week
     * @param $request
     * @param $id
     * @throws ApplicationErrorException
     */
    public function updateSchedule( $request, $id )
    {

        $user = Auth::user()->findOrFail($id);

//        $timezoneString = $this->getStringFromTimezoneKey($request->timezone);
        $timezoneString = $this->convertTimezone($request->timezone);

        // Process user's email reminder schedule
        $utcSchedule = $this->convertLocalScheduleToUTC( $timezoneString, $request->email_time, $request->reminder_days );

        $utcWeekdays = $utcSchedule['utcWeekdays'];

        if ( !is_array($utcWeekdays))
        {
            throw new ApplicationErrorException('Passed parameter is not of type array in: .'.__METHOD__);
        }

        // Process user's report schedule
        $reportDayArray = $this->transformRadioArrayToCheckboxArray( $request->report_day );

        $utcReportSchedule = $this->convertLocalScheduleToUTC( $timezoneString, $request->report_time, $reportDayArray );

        $utcReportDay = array_search( 1, $utcReportSchedule['utcWeekdays'] );

        // Retrieve user's schedule from the datastore
        if ( !$schedule = $user->schedule()->first() )
        {
            throw new ApplicationErrorException("No schedule found for user: $user->id");
        }

        // Set schedule parameters with our converted data, then save
        $schedule->timezone = $timezoneString;
        $schedule->utc_time = $utcSchedule['utcFormattedTime'];
        $schedule->sunday = $utcWeekdays['sunday'];
        $schedule->monday = $utcWeekdays['monday'];
        $schedule->tuesday = $utcWeekdays['tuesday'];
        $schedule->wednesday = $utcWeekdays['wednesday'];
        $schedule->thursday = $utcWeekdays['thursday'];
        $schedule->friday = $utcWeekdays['friday'];
        $schedule->saturday = $utcWeekdays['saturday'];
        $schedule->report_day = $utcReportDay;
        $schedule->report_utc_time = $utcReportSchedule['utcFormattedTime'];
        $schedule->save();

    }

    public function transformRadioArrayToCheckboxArray( $radioButtonArray )
    {
        $checkboxArray = [];

        foreach ($radioButtonArray as $key => $value)
        {
            $checkboxArray[$value] = 'on';
        }

        return $checkboxArray;
    }

    public function createReportDayObject( $dayString )
    {
        $dayObject = new \stdClass();

        $dayObject->sunday = 0;
        $dayObject->monday = 0;
        $dayObject->tuesday = 0;
        $dayObject->wednesday = 0;
        $dayObject->thursday = 0;
        $dayObject->friday = 0;
        $dayObject->saturday = 0;

        $dayObject->$dayString = 1;

        return $dayObject;
    }

    public function createReminderDaysObject( $schedule )
    {
        $daysObject = new \stdClass();

        $daysObject->sunday = $schedule->sunday ? 1 : 0;
        $daysObject->monday = $schedule->monday ? 1 : 0;
        $daysObject->tuesday = $schedule->tuesday ? 1 : 0;
        $daysObject->wednesday = $schedule->wednesday ? 1 : 0;
        $daysObject->thursday = $schedule->thursday ? 1 : 0;
        $daysObject->friday = $schedule->friday ? 1 : 0;
        $daysObject->saturday = $schedule->saturday ? 1 : 0;

        return $daysObject;
    }


    public function createReminderDaysArray( $daysArray )
    {
        $modDaysArray = [];
        $days = ['sunday','monday','tuesday','wednesday','thursday','friday','saturday'];

        foreach ($days as $key => $day)
        {
            if ( $daysArray[$day] != 0)
            {
                $modDaysArray[$day] = 'on';
            }
        }

        return $modDaysArray;
    }

    public function convertTimezone( $timezone )
    {
        if ( empty($timezone) || !( is_string($timezone) || is_int( (int)$timezone ) ) )
        {
            throw new ApplicationErrorException("Invalid or missing timezone value in: ".__METHOD__);
        }

        $tzList = DateTimeZone::listIdentifiers(DateTimeZone::ALL);

        if ( is_numeric( $timezone ) )
        {
            // Timezone key given, so return a string
            $convertedTimezone = $tzList[$timezone];
        }
        else
        {
            // Timezone string given, so return a key
            $convertedTimezone = array_search($timezone, $tzList);
        }

        if (!$convertedTimezone)
        {
            throw new ApplicationErrorException("Timezone not found.");
        }

        return $convertedTimezone;
    }


    public function convertUTCToLocalSchedule( $timezoneString, $utcFormattedTime, $daysObject )
    {
        $utcTimeArray = explode( ':', $utcFormattedTime );

        $utcDate = Carbon::create(
            Config::get('constants.base_date.year'),
            Config::get('constants.base_date.month'),
            Config::get('constants.base_date.day'),
            $utcTimeArray[0], $utcTimeArray[1], $utcTimeArray[2],
            'UTC');

        $localDate = $utcDate->copy()->setTimezone($timezoneString);

        $utcWeekdays = $this->createDaysArrayFromObject( $daysObject );

        if ($utcDate->day > $localDate->day)
            // The calendar day for the utc time is ahead of the user timezone day,
            // so shift the days back by one that are presented to the user
        {
            $localWeekdays = $this->assignDaysToArray( $utcWeekdays, -1 );

        } elseif ($utcDate->day < $localDate->day) {
            $localWeekdays = $this->assignDaysToArray( $utcWeekdays, 1 );

        } else {
            $localWeekdays = $this->assignDaysToArray( $utcWeekdays, 0 );
        }

        $localHour = $localDate->format("H");

        return [
            'localWeekdays' => $localWeekdays,
            'localHour'    =>  $localHour,
        ];
    }


    public function convertLocalScheduleToUTC( $timezoneString, $localTimeHour, $weekdaysArray )
    {
        if ( !is_integer((int)$localTimeHour) )
        {
            throw new ApplicationErrorException('Passed time argument is not a valid hour.');
        }

        // TODO: pull this out to its own method?
        $localDate = Carbon::create(
            Config::get('constants.base_date.year'),
            Config::get('constants.base_date.month'),
            Config::get('constants.base_date.day'),
            $localTimeHour, 0, 0,
            $timezoneString);

        $utcDate = $localDate->copy()->setTimezone('UTC');

        $localWeekdays = $this->processWeekdaysArray( $weekdaysArray );

        if ($utcDate->day > $localDate->day)
        {
            // UTC date is after the local date - shift days forward by one
            $utcWeekdays = $this->assignDaysToArray( $localWeekdays, 1 );

        }
        elseif ($utcDate->day < $localDate->day)
        {
            // UTC date is before user date - shift days back by one
            $utcWeekdays = $this->assignDaysToArray( $localWeekdays, -1 );

        }
        else
        {
            // UTC and Local dates are the same - don't shift
            $utcWeekdays = $this->assignDaysToArray( $localWeekdays, 0 );
        }

//        $utcArray['utc_time'] = $utcDate->format("H:i:s");
        $utcFormattedTime = $utcDate->format("H:i:s");

        //return $utcArray;
        return [
            'utcWeekdays'       =>   $utcWeekdays,
            'utcFormattedTime'  =>  $utcFormattedTime
        ];

    }


    public function createDaysArrayFromObject( $daysObject )
    {
        if (! is_object($daysObject))
        {
            throw new ApplicationErrorException('Passed parameter is not of type object.');
        }

        $user_days = array();
        $user_days[0]    =  $daysObject->sunday ? 1 : 0;
        $user_days[1]    =  $daysObject->monday ? 1 : 0;
        $user_days[2]    =  $daysObject->tuesday ? 1 : 0;
        $user_days[3]    =  $daysObject->wednesday ? 1 : 0;
        $user_days[4]    =  $daysObject->thursday ? 1 : 0;
        $user_days[5]    =  $daysObject->friday ? 1 : 0;
        $user_days[6]    =  $daysObject->saturday ? 1 : 0;

        return $user_days;
    }
    
    public function processWeekdaysArray( $weekdaysArray )
    {
        if (empty($weekdaysArray))
        {
            // The user cleared out all checkboxes or nothing (null) was submitted
            $weekdaysArray = [];
        }

        if (! is_array($weekdaysArray))
        {
            throw new ApplicationErrorException('Passed parameter is not of type array in: '.__METHOD__);
        }
        
        $weekdays[0]    =  isset($weekdaysArray['sunday']) ? 1 : 0;
        $weekdays[1]    =  isset($weekdaysArray['monday']) ? 1 : 0;
        $weekdays[2]    =  isset($weekdaysArray['tuesday'])? 1 : 0;
        $weekdays[3]    =  isset($weekdaysArray['wednesday']) ? 1 : 0;
        $weekdays[4]    =  isset($weekdaysArray['thursday']) ? 1 : 0;
        $weekdays[5]    =  isset($weekdaysArray['friday']) ? 1 : 0;
        $weekdays[6]    =  isset($weekdaysArray['saturday']) ? 1 : 0;

        return $weekdays;
        
    }

    public function assignDaysToArray(array $days_array, $offset = 0)
    {

        if ($offset > 0) {
            // Shift forward by one
            $last = array_pop($days_array);
            array_unshift($days_array, $last);

        } elseif ($offset < 0) {
            // Shift back by one
            $first = array_shift($days_array);
            array_push($days_array, $first);
        } else {
            // Do nothing
        }

        $utc_days = array();
        $utc_days['sunday']    =  $days_array[0];
        $utc_days['monday']    =  $days_array[1];
        $utc_days['tuesday']    =  $days_array[2];
        $utc_days['wednesday']    =  $days_array[3];
        $utc_days['thursday']    =  $days_array[4];
        $utc_days['friday']    =  $days_array[5];
        $utc_days['saturday']    =  $days_array[6];

        return $utc_days;
    }

    public function getHoursSelectArray()
    {
        $hours = [
            '4' =>  '4am',
            '5' =>  '5am',
            '6' =>  '6am',
            '7' =>  '7am',
            '8' =>  '8am',
            '9' =>  '9am',
            '10' =>  '10am',
            '11' =>  '11am',
            '12' =>  '12pm',
            '13' =>  '1pm',
            '14' =>  '2pm',
            '15' =>  '3pm',
            '16' =>  '4pm',
            '17' =>  '5pm',
            '18' =>  '6pm',
            '19' =>  '7pm',
            '20' =>  '8pm',
            '21' =>  '9pm',
            '22' =>  '10pm',
            '23' =>  '11pm'
        ];
        return $hours;
    }

    //echo '<pre>' . var_export($utc_time, true) . '</pre>';


}