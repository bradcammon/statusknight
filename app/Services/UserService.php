<?php

namespace App\Services;


use App\Contracts\UserServiceInterface;
use App\Exceptions\ApplicationErrorException;
use Auth;
use Hash;
use App\Schedule;
use App\User;
use App\Team;
use DB;
use Carbon\Carbon;


class UserService implements UserServiceInterface
{

    /**
     * Update the user model.
     *
     * @param $request
     * @param $id
     */
    public function updateUser( $request, $id )
    {
        $user = Auth::user()->findOrFail($id);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

    }

    public function updatePassword( $request, $id )
    {
        $response = [];
        $user = Auth::user()->findOrFail($id);

        // Process a password reset
        if (!Hash::check($request->old_password, $user->password))
        {
            return $response = [
                'password' => 'These credentials do not match our records.'
            ];
        }

        $user->password = Hash::make($request->new_password);
        $user->save();
        return $response;
    }

    public function updateTeam( $request, $id )
    {
        $user = Auth::user()->findOrFail($id);
        $team = $user->ownedTeams()->first();
        $team->name = $request->name;
        $team->save();
    }

    public function showUserSettings()
    {
        $user = Auth::user();

        // A user can only have one team currently (relationship defined in the User model)
        //, so the result of the following query should be one record.
        //TODO: Make this team-centric (i.e. user could have n teams)

        if (! $team = $user->ownedTeams()->first() )
        {
            throw new ApplicationErrorException("No team found for user: $user->id");
        }

        return compact('user', 'team');
    }

    public function createUserAndRelatedModels( array $data )
    {
        // Create an empty user object
        $user = new \stdClass();

        // Passing the above $user object by reference, so changes are made to the original.
        // That way, it can be returned to the calling method
        DB::transaction(function() use ($data, &$user) {
            $user =  User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'timezone_flag' =>  1
            ]);

            // Now that user has been created, create a personal team for them
            $team = Team::create([
                'name' => $data['name'].' personal',
                'owner_user_id' => $user->id
            ]);

            // Now add the relationship to the team_user mapping table
            //, thus adding the user as a member of their own team.
            $user->teams()->attach($team->id);

            //Create a default schedule for the user
            $schedule = Schedule::create([
                'user_id'   =>  $user->id
            ]);
        });

        return $user;
    }
    public function getReport ( $startDate, $endDate )
    {
        $user = Auth::user();

        if (!isset($startDate) || !isset($endDate))
        {
            $dates = $this->getDefaultReportDates($user->id);
            $startDate = $dates['startDate'];
            $endDate = $dates['endDate'];
        }


        $items = Auth::user()->items()
                ->select('raw_text', 'status_date')
                ->where('status_date', '>=', $startDate)
                ->where('status_date', '<=', $endDate)
                ->orderBy('status_date', 'asc')
                ->get();

        return compact('startDate', 'endDate', 'items', 'user');

    }

    private function getDefaultReportDates($id)
    {
        $dateDelta = 6;
        $user = Auth::user()->findOrFail($id);

        // Get dates based on user's weekly status preference
        $schedule = $user->schedule()->first();
        $schedService = new ScheduleService();

        $reportDayObject = $schedService->createReportDayObject($schedule->report_day);
        $localSchedule = $schedService->convertUTCToLocalSchedule($schedule->timezone, $schedule->report_utc_time, $reportDayObject);
        $localReportDay = array_search( 1, $localSchedule['localWeekdays'] );
        $days = ['sunday','monday','tuesday','wednesday','thursday','friday','saturday'];
        $dayOfWeekInteger = array_search($localReportDay,$days);

        $dt = Carbon::now( $schedule->timezone );

        // Get the last date of the user's specified reporting day
        // $dayOfWeekInteger is used in place of a Carbon constant,
        // e.g.: Carbon::WEDNESDAY, which returns int(3)
        $startDate = $dt->previous($dayOfWeekInteger)->addDay()->toDateString();
        $endDate = $dt->addDays($dateDelta)->toDateString();

        return [
            'startDate' => $startDate,
            'endDate'   =>  $endDate
        ];

    }



    //echo '<pre>' . var_export($utc_time, true) . '</pre>';


}