<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    /**
     * Get the users associated with the given team.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    /**
     * Fillable fields for a new Team.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'type',
        'owner_user_id'
    ];

}
