# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'statusknight'
set :repo_url, 'git@bitbucket.org:bradcammon/statusknight.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
 set :deploy_to, '/var/www/html/statusknight/deploy'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

# Set the components directory
components_dir = '/var/www/html/statusknight/components'
set :components_dir, components_dir

namespace :deploy do

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end

# Devops commands
namespace :ops do

  desc 'Copy non-git files to servers.'
  task :put_components do
    on roles(:web), in: :sequence, wait: 1 do
      system("tar -zcf ./build/vendor.tar.gz ./vendor ")
      upload! './build/vendor.tar.gz', "#{components_dir}", :recursive => true
      execute "cd #{components_dir}
      tar -zxf ./vendor.tar.gz"
    end
  end
end

namespace :laravel do

    desc "Run Laravel Artisan migrate task."
    task :migrate do
        on roles(:web), in: :sequence, wait: 5 do
            within release_path  do
                execute :php, "artisan migrate"
            end
        end
    end

    desc "Optimize Laravel Class Loader"
    task :optimize do
        on roles(:web), in: :sequence, wait: 5 do
            within release_path  do
                execute :php, "artisan clear-compiled"
                execute :php, "artisan optimize"
            end
        end
    end

    desc "Setup Laravel folder permissions"
    task :permissions do
        on roles(:web), in: :sequence, wait: 5 do
            within release_path  do
                execute :chmod, "u+x artisan"
                execute :chmod, "-R 775 storage/logs"
            end
        end
    end

end


namespace :deploy do
    #after :updated, "laravel:migrate"
    after :updated, "laravel:permissions"
    after :published, "laravel:optimize"
end



