<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyDefaultsOnSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schedules', function (Blueprint $table) {
            $table->boolean('sunday')->default(0)->change();
            $table->boolean('monday')->default(0)->change();
            $table->boolean('tuesday')->default(1)->change();
            $table->boolean('wednesday')->default(1)->change();
            $table->boolean('thursday')->default(1)->change();
            $table->boolean('friday')->default(1)->change();
            $table->boolean('saturday')->default(1)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schedules', function (Blueprint $table) {
            $table->boolean('sunday')->default(0)->change();
            $table->boolean('monday')->default(1)->change();
            $table->boolean('tuesday')->default(1)->change();
            $table->boolean('wednesday')->default(1)->change();
            $table->boolean('thursday')->default(1)->change();
            $table->boolean('friday')->default(1)->change();
            $table->boolean('saturday')->default(0)->change();
        });
    }
}
