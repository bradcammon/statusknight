jQuery(document).ready( function($) {

    var ReportModule = (function() {

        /** VARIABLES **/

        // Miscellaneous variables
        var dateFormat = "yy-mm-dd";
        var startDefaultDate = "-7";
        var endDefaultDate = "0";
        var numberOfMonths = 1;
        var headers = {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')};
        var hide = 'hide';

        // DOM Selector variables
        var startDateField = $('#startDate');
        var endDateField = $('#endDate');


        // URL variables
        //var getItemsUrl = '/item';

        /** METHODS **/


        var initStartDateField = function() {
            startDateField.datepicker({
                dateFormat: dateFormat,
                defaultDate: startDefaultDate,
                numberOfMonths: numberOfMonths,
                onClose: function( selectedDate ) {
                    endDateField.datepicker( "option", "minDate", selectedDate );
                }
            });
        };

        var initEndDateField = function() {
            endDateField.datepicker({
                dateFormat: dateFormat,
                defaultDate: endDefaultDate,
                numberOfMonths: numberOfMonths,
                onClose: function( selectedDate ) {
                    startDateField.datepicker( "option", "maxDate", selectedDate );
                }
            });
        };


        /**
         * Display jQuery ajax errors in the view.
         *
         * @param jq
         * @param status
         * @param message
         */
        var displayErrors = function( jq, status, message ) {
            //var error = "<li>" + jq.responseText + "</li>";
            var error = "<li>" + jq.statusText + "</li>";
            errorsContainer.removeClass("hidden");
            errorsContainer.append(error);
        };

        /**
         * Display custom server errors in the view.
         *
         * @param message
         */
        var displayCustomServerErrors = function( message ) {
            var error = "<li>" + message + "</li>";
            errorsContainer.removeClass("hidden");
            errorsContainer.append(error);
        };

        /**
         * Log ajax errors.
         * TODO: This should really push errors to the server...
         *
         * @param jq
         * @param status
         * @param message
         */
        var logErrors = function( jq, status, message ) {
            //console.log('A jQuery error has occurred. Status: ' + status + ' - Message: ' + message);
            //console.log(jq);
        };

        /**
         * Empties and hides the error container
         */
        var cleanUpErrorsContainer = function() {
            errorsContainer.empty();
            errorsContainer.addClass("hidden");
        };

        /**
         * Main 'bootstrap' function used to setup the initial view
         *
         */
        var loadMainView = function() {
            initStartDateField();
            initEndDateField();

        };

        /** EVENT LISTENERS **/

        // Listen for clicks to create a new Item
        //createNewItemButton.click( storeNewItem );


        /** PUBLIC API **/

        return {
            loadMainView: loadMainView
        };

    })();

     // Calls the loadMainView() public function whenever this script is loaded
    ReportModule.loadMainView();

});

jQuery(document).ready( function($) {

    var StatusModule = (function() {

        /** VARIABLES **/

        // Miscellaneous variables
        var headers = {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')};
        var deleteIcon = '<a href="#"><span class="glyphicon default-glyph-color glyphicon-trash" aria-hidden="true"></span></a>';
        var editIcon = '<a href="#"><span class="glyphicon default-glyph-color glyphicon-pencil" aria-hidden="true"></span></a>';
        var defaultItemIcon = '<span class="glyphicon default-glyph-color glyphicon-minus" aria-hidden="true"></span>';
        var hide = 'hide';

        // DOM Selector variables
        var dateContainer = $('#showdate');
        var calendarContainer = $('#datepicker');
        var hiddenDateFormField = $('#status_date');
        var statusItemField = $('#item');
        var itemsContainer = $('#list_ul');
        var outerItemsContainer = $('#list_ul');
        var loaderGif = $('.loader');
        var createNewItemButton = $('#create_button');
        var errorsContainer = $("#create_errors_ajax");
        var deleteItemSelector = $('.glyphicon-trash');
        var deleteItemSelectorString = '.glyphicon-trash';
        var editItemSelector = $('.glyphicon-pencil');
        var editItemSelectorString = '.glyphicon-pencil';
        var editContainerSelectorString = '.edit-container';
        var itemTextContainerSelectorString = '.item-text';
        var editCancelButtonSelectorString = '.edit-button-cancel';
        var editSubmitButtonSelectorString = '.edit-button-submit';
        var editFieldSelectorString = 'input.form-control';
        var timezoneCheckSelector = $('#timezone_check');
        var timezoneAlertSelector = '#timezone-alert';
        var itemIconSelectorString = '.item-icon';
        var actionIconSelectorString = '.action-tools';

        // URL variables
        var getItemsUrl = '/item';
        var storeItemsUrl = '/item';
        var deleteItemUrlBase = '/item/';
        var editItemUrlBase = '/item/';
        var storeTimezoneUrl = '/schedule/timezone'

        /** METHODS **/

        /**
         * Get today's date based on the system clock
         *
         * @returns {string}
         */
        var getTodaysDate = function() {
            var today = new Date();
            var todayDate = today.getDate();
            var todayMonth = 1 + (today.getMonth()); // jan == 0 , so add 1
            var todayYear = today.getFullYear();
            var todayFullDate = todayYear + "-" + todayMonth + "-" + todayDate;
            return todayFullDate;
        };

        /**
         * Display a date string in a container in the DOM
         * @param dateString
         */
        var displayDate = function(dateString) {
            var dateString = (typeof dateString !== 'undefined') ? dateString : getTodaysDate();
            dateContainer.html(dateString);
        };

        var getUserTimeZone = function() {
            var tz_check = timezoneCheckSelector.text();
            if (tz_check == 1 ) {
                var tz = jstz.determine();
                var tzName = tz.name();
                //console.log(tzName);
                sendTimezoneToServer( tzName );
            } else {
                //console.log('false');
            }
        };

        var displayTimezoneAlert = function() {
            //timezoneAlertSelector.removeClass( hide );
            $('#timezone-alert').removeClass( hide );
        }

        var sendTimezoneToServer = function( timezoneString ) {
            $.ajax({
                url: storeTimezoneUrl,
                method: 'POST',
                dataType: 'json',
                headers: headers,
                data: { timezone:timezoneString },
                success: function( result ) {
                    if (result.success != 'false') {
                        //console.log('successfully updated server')
                        //console.log( result );
                        //flash message to user
                        $('#user-timezone').text( result.timezone );

                        displayTimezoneAlert();

                    } else {
                        displayCustomServerErrors( result.message );
                    }

                },
                error: function( jq, status, message ) {
                    logErrors( jq, status, message );

                    //var errorText = jq.responseText;
                    //displayErrors( jq, status, message );
                }
            });

        };

        /**
         * Write a date string to a hidden form field
         * @param dateString
         */
        var updateHiddenDateFormField = function(dateString) {
            var dateString = (typeof dateString !== 'undefined') ? dateString : getTodaysDate();
            hiddenDateFormField.val(dateString);
        };

        /**
         * Displays the jQuery UI datepicker widget
         */
        var displayCalendar = function() {
            calendarContainer.datepicker({
                dateFormat: "yy-mm-dd",
                gotoCurrent: true,
                onSelect: function (dateText, inst) { //when a date is selected
                    displayDate(dateText);
                    updateHiddenDateFormField(dateText);
                    getItemsByDate(dateText);
                }
            });
        };

        /**
         * An AJAX GET request to retrieve all items for a specific date
         *
         * @param dateText
         */
        var getItemsByDate = function( dateText ) {
            $.ajax({
                url: getItemsUrl,
                method: 'GET',
                data: 'date=' +dateText,
                dataType: 'json',
                success: function(result) {
                    displayRetrievedItems(result);
                },
                error: function(jq, status, message) {
                    //console.log('A jQuery error has occurred. Status: ' + status + ' - Message: ' + message);
                    logErrors( jq, status, message );

                    var errorText = jq.responseText;
                    displayErrors( jq, status, message );
                },
                beforeSend: function() {
                    itemsContainer.empty();// make these more modular & decoupled?
                    //loaderGif.show();
                    itemsContainer.spin('small', '#3B4444');
                },
                complete: function() {
                    //loaderGif.hide();
                    itemsContainer.spin(false);
                }
            });
        };

        /**
         * Loops through an array of objects and displays them in the view.
         * TODO: De-couple from presentation/html somehow?
         *
         * @param items
         */
        var displayRetrievedItems = function( items ) {
            $.each(items, function (index, value) {
                var item_html =
                    '<li data-item-id="' +value.id+ '">' +
                    '<div class="row">' +
                        '<div class="col-xs-2 col-sm-1 col-md-1 item-icon">' + defaultItemIcon + '</div>' +
                        '<div class="col-xs-10 col-sm-8 col-md-9 item-text">' + value.raw_text + '</div>' +
                        '<div class="hidden-xs col-sm-3 col-md-2 action-tools">' + deleteIcon + editIcon + '</div>' +
                        '<div class="edit-container form-group hide">' +
                            '<div class="row">' +
                                '<div class="hidden-xs col-xs-10 col-xs-offset-1">' +
                                    '<input class="form-control" type="text" value="' + value.raw_text + '">' +
                                '</div>' +
                            '</div>' +
                            '<div class="row">' +
                                '<div class="hidden-xs col-xs-10 col-xs-offset-1 text-right">' +
                                    //'<input class="btn btn-default edit-button-cancel" type="button" value="Cancel">' +
                                    '<button class="btn btn-default edit-button-cancel" type="button">Cancel</button>' +
                                    //'<input class="btn btn-primary edit-button-submit" type="button" value="Submit">' +
                                    '<button class="btn btn-primary edit-button-submit" type="button">Submit</button>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '</li>';
                itemsContainer.append(item_html);
            });
        };

        /**
         * Stores a newly entered status item to the server
         *
         * @param e
         */
        var storeNewItem = function( e ) {
            e.preventDefault(); //prevent html form from submitting
            $.ajax({
                url: storeItemsUrl,
                method: 'POST',
                dataType: 'json',
                headers: headers,
                data: { item:statusItemField.val(), status_date:hiddenDateFormField.val() },
                success: function( result ) {
                    if (result.success != 'false') {
                        cleanUpErrorsContainer();
                        displayRetrievedItems( result );
                        statusItemField.val( '' );
                    } else {
                        displayCustomServerErrors( result.message );
                    }

                },
                error: function( jq, status, message ) {
                    logErrors( jq, status, message );

                    var errorText = jq.responseText;
                    displayErrors( jq, status, message );
                },
                beforeSend: function(){
                    $('#create_button span').text('').spin('small', '#fff');
                },
                complete: function(){
                    $('#create_button span').spin(false).text('Submit');
                }
            });

        };

        /**
         * Deletes a specific item from the server
         */
        var deleteItem = function() {
            var itemIdToDelete = $( this ).parents( "li" ).data( "itemId");
            var thisListItem = $( this ).parents( "li" );
            var deleteElement = $(this);

            $.ajax({
                url: deleteItemUrlBase + itemIdToDelete,
                method: 'DELETE',
                dataType: 'json',
                headers: headers,
                success: function( result ) {
                    if (result.success != 'false') {
                        thisListItem.remove();
                    } else {
                        displayCustomServerErrors( result.message );
                    }
                },
                error: function( jq, status, message ) {
                    logErrors( jq, status, message );

                    var errorText = jq.responseText;
                    displayErrors( jq, status, message );
                },
                beforeSend: function(){
                    //console.log(deleteElement);
                    deleteElement.css('color','#D90429');
                    //deleteElement.removeClass('glyphicon-trash');
                },
                complete: function(){

                }

            });

        }

        /**
         * Updates a status item on the server
         */
        var submitEditedItem = function() {
            var thisListItem = $(this).parents("li");
            var itemId = $( this ).parents( "li" ).data( "itemId");
            var text = thisListItem.find("input[type='text']").val();
            var submitButton = thisListItem.find("button.btn.btn-primary.edit-button-submit");
            //console.log(submitButton);

            $.ajax({
                url: editItemUrlBase + itemId,
                method: 'PUT',
                headers: headers,
                dataType: 'json',
                data: {item: text},
                success: function ( result ) {
                    //console.log('success');
                    thisListItem.find( itemTextContainerSelectorString ).text(text);
                    thisListItem.find( editContainerSelectorString ).addClass( hide );
                    thisListItem.find( itemTextContainerSelectorString ).removeClass( hide );
                    thisListItem.find( itemIconSelectorString ).removeClass( hide );
                    thisListItem.find( actionIconSelectorString ).removeClass( hide );

                },
                error: function( jq, status, message ) {
                    logErrors( jq, status, message );

                    var errorText = jq.responseText;
                    displayErrors( jq, status, message );
                },
                beforeSend: function(){
                    submitButton.text('').spin('small', '#fff');
                },
                complete: function(){
                    submitButton.spin(false).text('Submit');
                }
            });
        };

        /**
         * Enter into edit mode (i.e., display a text field containing the current item text
         */
        var displayItemEditMode = function() {
            var thisListItem = $( this ).parents( "li" );
            //console.log(thisListItem);
            thisListItem.find( editContainerSelectorString ).removeClass( hide );
            thisListItem.find( itemTextContainerSelectorString ).addClass( hide );
            thisListItem.find( itemIconSelectorString ).addClass( hide );
            thisListItem.find( actionIconSelectorString ).addClass( hide );
        };

        /**
         * Hides the edit textfield and returns to the normal item view
         */
        var cancelItemEditMode = function() {
            var thisListItem = $(this).parents("li");
            //console.log(thisListItem);
            thisListItem.find( editContainerSelectorString ).addClass( hide );
            thisListItem.find( itemTextContainerSelectorString ).removeClass( hide );
            thisListItem.find( itemIconSelectorString ).removeClass( hide );
            thisListItem.find( actionIconSelectorString ).removeClass( hide );
        };

        /**
         * Watches for keypresses in the edit textfield.  If the enter key is
         * pressed, then trigger a click on the edit submit button.
         *
         * @param e
         */
        var detectEditEnterKeypress = function( e ) {
            var key = e.which;
            if (key == 13) { //the "enter" key
                var thisListItem = $(this).parents(".edit-container");
                thisListItem.find(editSubmitButtonSelectorString).click();
            }
        }

        var detectCreateEnterKeypress = function( e ) {
            var key = e.which;
            if (key == 13) { //the "enter" key
                //$('#create_button').click();
                storeNewItem(e);

            }
        }

        /**
         * Display jQuery ajax errors in the view.
         *
         * @param jq
         * @param status
         * @param message
         */
        var displayErrors = function( jq, status, message ) {
            //var error = "<li>" + jq.responseText + "</li>";
            //var error = "<li>" + jq.statusText + "</li>";
            var error = '<span id="error_text">'+jq.statusText+'</span>';
            $('#error_text').remove();
            errorsContainer.removeClass("hidden");
            errorsContainer.append(error);
        };

        /**
         * Display custom server errors in the view.
         *
         * @param message
         */
        var displayCustomServerErrors = function( message ) {
            //var error = "<li>" + message + "</li>";
            var error = '<span id="error_text">'+message+'</span>';
            $('#error_text').remove();
            errorsContainer.removeClass("hidden");
            errorsContainer.append(error);
        };

        /**
         * Log ajax errors.
         * TODO: This should really push errors to the server...
         *
         * @param jq
         * @param status
         * @param message
         */
        var logErrors = function( jq, status, message ) {
            //console.log('A jQuery error has occurred. Status: ' + status + ' - Message: ' + message);
            //console.log(jq);
        };

        /**
         * Empties and hides the error container
         */
        var cleanUpErrorsContainer = function() {
            errorsContainer.empty();
            errorsContainer.addClass("hidden");
        };

        /**
         * Main 'bootstrap' function used to setup the initial view
         *
         */
        var loadMainView = function() {
            displayDate();
            $('#item_container').removeClass( hide );//outerItemsContainer.removeClass(hide) doesn't work...
            displayCalendar();
            updateHiddenDateFormField();
            getItemsByDate(getTodaysDate());
            getUserTimeZone();
        };

        /** SPINNER PRESETS **/

        $.fn.spin.presets.skdefault = {
            lines: 15,
            length: 23,
            width: 6,
            radius: 34,
        }

        /** EVENT LISTENERS **/

        // Listen for clicks to create a new Item
        createNewItemButton.click( storeNewItem );

        // Listen for the enter key being pressed in the create field
        $('#create_form').on( 'keypress', '#item', detectCreateEnterKeypress );

         // Listen for clicks to delete an Item.
        itemsContainer.on( 'click', deleteItemSelectorString, deleteItem );

        // Listen for clicks to edit an Item
        itemsContainer.on( 'click', editItemSelectorString, displayItemEditMode );

        // Listen for clicks on the edit field's cancel button
        itemsContainer.on( 'click', editCancelButtonSelectorString, cancelItemEditMode );

        // Listen for clicks on the edit field's submit button
        itemsContainer.on( 'click', editSubmitButtonSelectorString, submitEditedItem );

        // Listen for the enter key being pressed in the edit field
        itemsContainer.on( 'keypress', editFieldSelectorString, detectEditEnterKeypress );

        // Listen for clicks on any button with the .btn class, to remove the focus state
        // after being clicked.  Not a big deal, but removes the blue outline
        $(".btn").mouseup(function(){
            $(this).blur();
        })

        /** PUBLIC API **/

        return {
            loadMainView: loadMainView
        };

    })();

     // Calls the loadMainView() public function whenever this script is loaded

    StatusModule.loadMainView();

});

jQuery(document).ready( function($) {

    var Schedule = (function() {

        /** VARIABLES **/

        // Miscellaneous variables
        var headers = {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')};
        var hide = 'hide';

        // DOM Selector variables
        var startDateField = $('#startDate');
        var endDateField = $('#endDate');


        // URL variables
        //var getItemsUrl = '/item';

        /** METHODS **/


        var initStartDateField = function() {
            startDateField.datepicker({
                dateFormat: dateFormat,
                defaultDate: startDefaultDate,
                numberOfMonths: numberOfMonths,
                onClose: function( selectedDate ) {
                    endDateField.datepicker( "option", "minDate", selectedDate );
                }
            });
        };

        //var disableReminderSection = function() {
        //    $('#daily-email').prop('disabled', true);
        //}

        var initDailyEmailSection = function() {
            //console.log('init daily...');
            if ( $('#daily-email-optin').is(':checked') ) {
                $('#daily-email').prop('disabled', false);
            }
            else {
                $('#daily-email').prop('disabled', true);
            }
        };

        var toggleReminderSection = function() {
            if ( $('#daily-email-optin').is(':checked') ) {
                $('#daily-email').prop('disabled', false);
            }
            else {
                $('#daily-email').prop('disabled', true);
            }
        };

        /**
         * Display jQuery ajax errors in the view.
         *
         * @param jq
         * @param status
         * @param message
         */
        var displayErrors = function( jq, status, message ) {
            //var error = "<li>" + jq.responseText + "</li>";
            var error = "<li>" + jq.statusText + "</li>";
            errorsContainer.removeClass("hidden");
            errorsContainer.append(error);
        };

        /**
         * Display custom server errors in the view.
         *
         * @param message
         */
        var displayCustomServerErrors = function( message ) {
            var error = "<li>" + message + "</li>";
            errorsContainer.removeClass("hidden");
            errorsContainer.append(error);
        };

        /**
         * Log ajax errors.
         * TODO: This should really push errors to the server...
         *
         * @param jq
         * @param status
         * @param message
         */
        var logErrors = function( jq, status, message ) {
            //console.log('A jQuery error has occurred. Status: ' + status + ' - Message: ' + message);
            //console.log(jq);
        };

        /**
         * Empties and hides the error container
         */
        var cleanUpErrorsContainer = function() {
            errorsContainer.empty();
            errorsContainer.addClass("hidden");
        };

        /**
         * Main 'bootstrap' function used to setup the initial view
         *
         */
        var loadMainView = function() {
            //initDailyEmailSection();

        };

        /** EVENT LISTENERS **/

        // Listen for clicks to create a new Item
        //createNewItemButton.click( storeNewItem );
        $('#daily-email-optin').click(toggleReminderSection);


        /** PUBLIC API **/

        return {
            loadMainView: loadMainView
        };

    })();

     // Calls the loadMainView() public function whenever this script is loaded
    Schedule.loadMainView();

});

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-72788840-1', 'auto');
ga('send', 'pageview');
//# sourceMappingURL=all.js.map
