/**
 * Created by detroit on 12/1/15.
 */

jQuery( document ).ready(function($) {
    /**
     * setup the csrf token for ajax calls
     */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /**
     * get today's date for display and for server queries
     * @returns {string}
     */
    function getTodaysDate() {
        var today = new Date();
        var todayDate = today.getDate();//(1-31)
        var todayMonth = 1 + (today.getMonth());//(0-11)
        var todayYear = today.getFullYear();//yyyy
        var todayFullDate = todayYear + "-" + todayMonth + "-" + todayDate;
        return todayFullDate;
    }

    // this stuff happens on page load

    // insert today's date at the top of the view
    $('#showdate').html(getTodaysDate());

    // insert today's date into the form field
    $('#status_date').val(getTodaysDate());

    // get the items that match today's date and load the view
    $.ajax({
        url: '/item/ajax',
        method: 'GET',
        data: "date=" + getTodaysDate(),
        dataType: "json",
        success: function (result) {
            //console.log(result);
            //$('#item_list').html(result);
            $.each(result, function (index, value) {
                //console.log(index + ":" +value.raw_text);
                var item_html = "<li>" + value.raw_text + " (" + value.status_date + ")" + "</li>";
                $("#list_ul").append(item_html);


            });

        }
    });


    /**
     * display the calendar
     */
    $("#datepicker").datepicker({
//                beforeShowDay: function(date) {
//                    if($.inArray(date,my_dates)) {
//                        console.log('true');
//                        return [true, 'xxui-state-highlight', 'tooltip text'];
//                    } else {
//                        console.log('false');
//                        return [true, '', ''];
//                    }
//                },
        dateFormat: "yy-mm-dd",
        gotoCurrent: true,
        onSelect: function (dateText, inst) {
            $('#showdate').html(dateText);
            $('#status_date').val(dateText);
            getItemsByDate(dateText);
        }
    });


    /**
     * This function is called when a date on the calendar is selected
     * It uses ajax to retrieve items for the selected date then
     * display them
     *
     * @param dateText
     */
    function getItemsByDate(dateText) {
        var deleteIcon = '<a href="#"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
        var editIcon = '<a href="#"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>';
        $('#status_date').val(dateText);
        $.ajax({
            url: '/item/ajax',
            method: 'GET',
            data: "date=" + dateText,
            dataType: "json",
            success: function (result) {
                //console.log(result);
                //$('#showdate').html(result.date);
                $.each(result, function (index, value) {
                    //console.log(value);
                    //console.log(index + ":" +value.raw_text);
                    //var item_html = "<li>" + value.raw_text + " (" + value.status_date + ")" + "</li>";
                    var item_html = '<li data-item-id="' +value.id+ '">' +
                        '<div class="item-text">' + value.raw_text + '</div>' +
                        '<div class="action-tools">' + deleteIcon + editIcon + '</div>' +
                        '<div class="edit-container form-group hide">' +
                        '<input class="form-control" type="text" value="' + value.raw_text + '">' +
                        '<input class="btn btn-primary edit-button-submit" type="button" value="Submit">' +
                        '<input class="btn btn-default edit-button-cancel" type="button" value="Cancel">' +
                        '</div>' +
                        '</li>';
                    $("#list_ul").append(item_html);

                });
            },
            beforeSend: function(){
                $("#list_ul").empty();
                $('.loader').show()
            },
            complete: function(){
                $('.loader').hide();
            }

        });
    }


    /**
     * Delete an item when the trash icon is clicked
     */
    $("#list_ul").on('click', ".glyphicon-trash", function () {
        var itemId = $( this ).parents( "li" ).data( "itemId");
        var thisListItem = $( this ).parents( "li" );
        console.log(this);
        console.log(thisListItem);

        $.ajax({
            url: '/item/ajax/'+itemId,
            method: 'DELETE',
            success: function (result) {
                //console.log(result);
                thisListItem.remove();
            },
            error: function(jq, status, message) {
                console.log('A jQuery error has occurred. Status: ' + status + ' - Message: ' + message);
                //console.log(jq);
            }

        });

    });

    /**
     * Enter into edit mode when the pencil icon is clicked
     * i.e., display a textfield containing the current item text
     */
    $("#list_ul").on('click', ".glyphicon-pencil", function () {
        var itemId = $( this ).parents( "li" ).data( "itemId");
        var thisListItem = $( this ).parents( "li" );
        var editContainer = thisListItem.find(".edit-container");
        var itemText = thisListItem.find(".item-text");
        editContainer.removeClass("hide");
        itemText.addClass("hide");
        //editContainer.add(itemText).toggle();

    });

    // Clicking the 'Cancel' button under the edit text field hides the text field
    $("#list_ul").on('click', ".edit-button-cancel", function () {
        var thisListItem = $(this).parents("li");
        thisListItem.find(".edit-container").addClass("hide");
        thisListItem.find(".item-text").removeClass("hide");
    });

    // If the enter key is pressed in the edit field, a click on
    // the submit button is triggered
    $("#list_ul").on('keypress', 'input.form-control', function (e) {
        var key = e.which;
        if (key == 13) { //the "enter" key
            $(this).siblings(".edit-button-submit").click();
        }
    });

    /**
     * Posts the edited item back to the server, then
     * updates the list item in the view
     */
    $("#list_ul").on('click', ".edit-button-submit", function () {
        var thisListItem = $(this).parents("li");
        var itemId = $( this ).parents( "li" ).data( "itemId");
        var text = $(this).siblings("input[type='text']").val();
        //console.log(text);
        //console.log(itemId);
        $.ajax({
            url: '/item/ajax/'+itemId,
            method: 'PUT',
            data: {item: text},
            dataType: "json",
            success: function (result) {
                console.log(result);
                thisListItem.find(".item-text").text(text);
                thisListItem.find(".edit-container").addClass("hide");
                thisListItem.find(".item-text").removeClass("hide");


            },
            error: function (result) {

            }
        });

        //var thisListItem = $(this).parents("li");
        //
        //
    });

    /**
     * Create a new item and post it to the server
     */
    $("#create_button").click(function(e){
        e.preventDefault();
        //console.log("prevented!");
        //console.log(e);
        $.ajax({
            url: '/item/ajax_store',
            method: 'POST',
            //data: $("#create_form").serialize(),
            data: {item:$("#item").val(), status_date:$("#status_date").val()},
            //data:{item: "foo", status_date: "2015-11-27"},
            dataType: "json",
            success: function (result) {
                $("#create_errors_ajax").empty();
                $("#create_errors_ajax").addClass("hidden");
                //console.log(result);
                var item_html = "<li>"+"<div>" + result.raw_text + " (" + result.status_date + ")" + "</div><div><span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span></div>"+"</li>";
                $("#list_ul").append(item_html);
                $("#item").val('');

            },
            error: function(jq,status,message) {
                console.log('A jQuery error has occurred. Status: ' + status + ' - Message: ' + message);
                console.log(jq);
                console.log(jq.responseJSON);
                var error = "<li>" + jq.responseText + "</li>";
                console.log(error);
                $("#create_errors_ajax").removeClass("hidden");
                $("#create_errors_ajax").append(error);

            },
            beforeSend: function(){
                //$('.loader').show()
            },
            complete: function(){
                // $('.loader').hide();
            }

        });
    });

});