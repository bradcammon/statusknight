## Status Knight

Make Status Reporting Easy, and get back to doing what you enjoy! To check it out in action, visit [statusknight.com](http://statusknight.com). 


*Status Knight is a project based on the Laravel framework (5.1+).  It borrows shamelessly from IDoneThis.*

### Track your accomplishments on a daily basis

Now, you can produce detailed status reports easily. No more combing through past emails, code commits, etc. at the end of the week. By documenting a little bit each day, you'll be amazed at how much you're getting done each week!

### Once a week, receive a summary of what you got done over the past week

Receive an elegantly formatted recap of your accomplishments from the previous week, ready to be presented to anyone, from clients to managers.

### Share with your team, or just use it to track your own progress over time.

If you're working solo, seeing a steady stream of accomplishments can be highly rewarding, and motivate you to push towards the next milestone.

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Status Knight license is TBD.