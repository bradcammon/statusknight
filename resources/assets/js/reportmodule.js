jQuery(document).ready( function($) {

    var ReportModule = (function() {

        /** VARIABLES **/

        // Miscellaneous variables
        var dateFormat = "yy-mm-dd";
        var startDefaultDate = "-7";
        var endDefaultDate = "0";
        var numberOfMonths = 1;
        var headers = {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')};
        var hide = 'hide';

        // DOM Selector variables
        var startDateField = $('#startDate');
        var endDateField = $('#endDate');


        // URL variables
        //var getItemsUrl = '/item';

        /** METHODS **/


        var initStartDateField = function() {
            startDateField.datepicker({
                dateFormat: dateFormat,
                defaultDate: startDefaultDate,
                numberOfMonths: numberOfMonths,
                onClose: function( selectedDate ) {
                    endDateField.datepicker( "option", "minDate", selectedDate );
                }
            });
        };

        var initEndDateField = function() {
            endDateField.datepicker({
                dateFormat: dateFormat,
                defaultDate: endDefaultDate,
                numberOfMonths: numberOfMonths,
                onClose: function( selectedDate ) {
                    startDateField.datepicker( "option", "maxDate", selectedDate );
                }
            });
        };


        /**
         * Display jQuery ajax errors in the view.
         *
         * @param jq
         * @param status
         * @param message
         */
        var displayErrors = function( jq, status, message ) {
            //var error = "<li>" + jq.responseText + "</li>";
            var error = "<li>" + jq.statusText + "</li>";
            errorsContainer.removeClass("hidden");
            errorsContainer.append(error);
        };

        /**
         * Display custom server errors in the view.
         *
         * @param message
         */
        var displayCustomServerErrors = function( message ) {
            var error = "<li>" + message + "</li>";
            errorsContainer.removeClass("hidden");
            errorsContainer.append(error);
        };

        /**
         * Log ajax errors.
         * TODO: This should really push errors to the server...
         *
         * @param jq
         * @param status
         * @param message
         */
        var logErrors = function( jq, status, message ) {
            //console.log('A jQuery error has occurred. Status: ' + status + ' - Message: ' + message);
            //console.log(jq);
        };

        /**
         * Empties and hides the error container
         */
        var cleanUpErrorsContainer = function() {
            errorsContainer.empty();
            errorsContainer.addClass("hidden");
        };

        /**
         * Main 'bootstrap' function used to setup the initial view
         *
         */
        var loadMainView = function() {
            initStartDateField();
            initEndDateField();

        };

        /** EVENT LISTENERS **/

        // Listen for clicks to create a new Item
        //createNewItemButton.click( storeNewItem );


        /** PUBLIC API **/

        return {
            loadMainView: loadMainView
        };

    })();

     // Calls the loadMainView() public function whenever this script is loaded
    ReportModule.loadMainView();

});
