<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Status JSON Response Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for general error messages.
    |
    */

    'general_error' => 'An error occurred.',
    'item_deleted'  =>  'Item successfully deleted',
    'delete_exception'  =>  'Failed to delete item',



];
