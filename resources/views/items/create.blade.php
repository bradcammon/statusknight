@extends('layout')

@section('content')


<div id="datepicker"></div>

<div id="item_list" class="hide">
    <h2 class='page-heading'>xxx for <span id="showdate"></span></h2>
    <div class="loader">
            <img class="loading-image" src="/images/ajax-loader.gif" alt="loading..">
    </div>
    <ul id="list_ul">

    </ul>
</div>
<br class="clear">

<ul>
    @foreach ($items as $item)
        <li>
            {{ $item->raw_text }} - {{ $item->id }}
            {!! Form::open(['method' => 'delete', 'action' => ['ItemController@destroy', $item->id], 'style' => 'display:inline']) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
            <a href='{{ url( action('ItemController@edit', $item->id)) }}'>Edit</a>
        </li>
    @endforeach
</ul>

<div id="food">
    {!! Form::open(array('action' => 'ItemController@store', 'method' => 'post', 'id' => 'create_form')) !!}

    <div class="form-group">
        {!! Form::label('item', 'Status Item') !!}
        {!! Form::text('item', null, ['class' => 'form-control', 'id'=>'item', 'placeholder' => 'Enter your status item...']) !!}
    </div>
    <!--
    <div class="form-group">
        {!! Form::label('date', 'Date') !!}
        {!! Form::date('status_date', \Carbon\Carbon::now(), ['class' => 'form-control', 'id' =>'status_date']) !!}
    </div>
    -->

    {!! Form::hidden('status_date', null, ['id'=>'status_date']) !!}

    <div class='form-group'>
        {!! Form::submit('Submit', ['class' => 'btn btn-primary', 'id' =>'create_button']) !!}
    </div>

    {!! Form::close() !!}
</div>

<ul id='create_errors_ajax' class='alert alert-danger hidden'>

</ul>

@if ($errors->any())
    <ul id='create_errors' class='alert alert-danger'>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif

@endsection

