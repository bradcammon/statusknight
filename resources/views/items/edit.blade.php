@extends('layout')

@section('content')

<h1 class='page-heading'>Edit</h1>

{!! Form::open(array('action' => ['ItemController@update', $item->id], 'method' => 'put')) !!}

<div class="form-group">
    {!! Form::label('item', 'Status Item') !!}
    {!! Form::text('item', $item->raw_text, ['class' => 'form-control']) !!}
</div>

<div class='form-group'>
    {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
</div>

{!! Form::close() !!}
    
@if ($errors->any())
    <ul class='alert alert-danger'>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif

@endsection

