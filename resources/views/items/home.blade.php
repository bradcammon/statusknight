@extends('layout')

@section('content')

<div class="container">
    <div id="content-layout" class="row">

        <div id="datepicker-col" class="col-sm-4">
            <h3>{{ $teamName or 'My Team' }}</h3>
            <div id="datepicker"></div>
        </div>

        <div class="col-sm-8">
            <div id="item_container" class="hide">
                <h3 class='page-heading'>Status for <span id="showdate"></span></h3>
                <div id="item_list" class="row">
                    <div class="col-xs-12">
                        <ul id="list_ul" class="list-unstyled"></ul>
                    </div>
                </div>
            </div>

            <div id="new-item-form">
                {!! Form::open(array('action' => 'ItemController@store', 'method' => 'post', 'id' => 'create_form')) !!}

                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            {!! Form::label('item', 'Status Item') !!}
                            {!! Form::text('item', null, ['class' => 'form-control', 'id'=>'item', 'placeholder' => 'Enter your status item...']) !!}
                        </div>

                        {!! Form::hidden('status_date', null, ['id'=>'status_date']) !!}
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 text-right">
                        <div class='form-group'>
                            {!! Form::button('<span>Submit</span>', ['class' => 'btn btn-primary', 'id' =>'create_button']) !!}
                        </div>
                    </div>
                </div>

                {!! Form::close() !!}
            </div>

            <div id='create_errors_ajax' class='alert alert-danger hidden'>
                <span class="glyphicon glyphicon-exclamation-sign text-danger" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
            </div>

            @if ($errors->any())
                <ul id='create_errors' class='alert alert-danger' role="alert">
                    @foreach ($errors->all() as $error)
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Error:</span>
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

        </div>
    </div>
</div>


@endsection

