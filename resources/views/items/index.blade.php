@extends('layout')

@section('content')
<h1 class="page-heading">Your Items</h1>

<table class="table table-striped table-bordered">
    <thead>
        <th>Item</th>
        <th>Id</th>
        <th>Date</th>
    </thead>
    
    <tbody>
        @foreach ($items as $item)
        <tr>
            <td>{{ $item->raw_text }}</td>
            <td>{{ $item->id }}</td>
            <td>{{ $item->created_at->diffForHumans() }}</td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection

