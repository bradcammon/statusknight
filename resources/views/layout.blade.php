<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Status Knight</title>

	<link href="/css/bootstrap.min.css" rel="stylesheet">
	<link href="/css/jquery-ui.min.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">

	<!-- Fonts -->
	<!--<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>-->
	{{--<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">--}}
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<!--<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>-->
		<!--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->
	<![endif]-->

	<!-- Favicon stuff -->
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/manifest.json">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
        
        
</head>
<body>
    <meta name="csrf-token" content="{{ csrf_token() }}">

        @include('partials.nav')
        <div class='container'>

			@if (Session::has('flash_message_success'))
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<div class="alert alert-success alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							{{ Session::get('flash_message_success') }}
						</div>
					</div>
				</div>
			@endif

            @yield('content')
        </div>

		@if(auth()->user())
			<div id="timezone_check" style="display: none;">{{ auth()->user()->timezone_flag }}</div>
		@endif

	<!-- Scripts -->
	<script src="/js/jquery.min.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/jquery-ui.min.js"></script>
	<script src="/js/jstz.min.js"></script>
	<script src="/js/spin.min.js"></script>
	<script src="/js/jquery.spin.js"></script>
	<script src="/js/all.js"></script>


</body>
</html>