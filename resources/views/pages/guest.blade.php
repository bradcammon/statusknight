@extends('layout')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Welcome To Status Knight!</div>

				<div class="panel-body">
					<blockquote>
						<p>
							The agony of compiling a status report is now a relic of the past!
						</p>
						<footer>Anonymous</footer>
					</blockquote>

					<ul>
						<li>Track your accomplishments on a daily basis.</li>
						<li>Once a week, receive a summary of what you got done over the past week.</li>
						<li>Share with your team, or just use it to track your own progress over time.</li>
					</ul>

					Now, you can produce detailed status reports
					easily.  No more combing through past emails, code commits, etc.  By documenting a little
					bit each day, you'll be amazed at how much you're getting done each week!
				</div>
			</div>
		</div>
	</div>
</div>
@endsection