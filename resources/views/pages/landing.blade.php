<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Status Knight</title>

	<!-- Bootstrap Core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom CSS -->
	<link href="css/landing-page.css" rel="stylesheet">
	<link href="/css/app.css" rel="stylesheet">

	<!-- Custom Fonts -->
	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- Favicon stuff -->
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/manifest.json">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">

</head>

<body>

<!-- Navigation -->
{{--<nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">--}}
	{{--<div class="container topnav">--}}
		{{--<!-- Brand and toggle get grouped for better mobile display -->--}}
		{{--<div class="navbar-header">--}}
			{{--<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">--}}
				{{--<span class="sr-only">Toggle navigation</span>--}}
				{{--<span class="icon-bar"></span>--}}
				{{--<span class="icon-bar"></span>--}}
				{{--<span class="icon-bar"></span>--}}
			{{--</button>--}}
			{{--<a class="navbar-brand topnav" href="#">Start Bootstrap</a>--}}
		{{--</div>--}}
		{{--<!-- Collect the nav links, forms, and other content for toggling -->--}}
		{{--<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">--}}
			{{--<ul class="nav navbar-nav navbar-right">--}}
				{{--<li>--}}
					{{--<a href="#about">About</a>--}}
				{{--</li>--}}
				{{--<li>--}}
					{{--<a href="#services">Services</a>--}}
				{{--</li>--}}
				{{--<li>--}}
					{{--<a href="#contact">Contact</a>--}}
				{{--</li>--}}
			{{--</ul>--}}
		{{--</div>--}}
		{{--<!-- /.navbar-collapse -->--}}
	{{--</div>--}}
	{{--<!-- /.container -->--}}
{{--</nav>--}}


{{--@include('partials.nav')--}}

<nav class="navbar navbar-default navbar-fixed-top topnav">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
				<span class="sr-only">Toggle Navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/"><img src="/images/statusknight-logo.png" alt="Status Knight Logo"> </a>
		</div>

		<div class="collapse navbar-collapse" id="navbar">
			<ul class="nav navbar-nav">
				@if(auth()->guest())
				@else
					<li><a href="{{ url('/') }}">Home</a></li>
					<li><a href="{{ url('/user/report') }}">Report</a></li>
				@endif
			</ul>

			<ul class="nav navbar-nav navbar-right">
				@if(auth()->guest())
					<li><a href="{{ url('/auth/register') }}">Signup</a></li>
					<li><a href="{{ url('/auth/login') }}">Login</a></li>
				@else
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
							{{ auth()->user()->name }} <span class="caret"></span>
						</a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="{{ url('/user/settings') }}">Settings</a></li>
							<li><a href="{{ url('/schedule') }}">Schedule</a></li>
							<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
						</ul>
					</li>
				@endif
			</ul>
		</div>
	</div>
</nav>

<!-- Header -->
<div class="intro-header">
	<div class="container">

		<div class="row">
			<div class="col-lg-12">
				<div class="intro-message">
					<h1>Make Status Reporting Easy</h1>
					<h3>...and get back to doing what you enjoy.</h3>
					<hr class="intro-divider">
					{{--<ul class="list-inline intro-social-buttons">--}}
						{{--<li>--}}
							{{--<a href="https://twitter.com/SBootstrap" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a>--}}
						{{--</li>--}}
						{{--<li>--}}
							{{--<a href="https://github.com/IronSummitMedia/startbootstrap" class="btn btn-default btn-lg"><i class="fa fa-github fa-fw"></i> <span class="network-name">Github</span></a>--}}
						{{--</li>--}}
						{{--<li>--}}
							{{--<a href="#" class="btn btn-default btn-lg"><i class="fa fa-linkedin fa-fw"></i> <span class="network-name">Linkedin</span></a>--}}
						{{--</li>--}}
					{{--</ul>--}}
				</div>
			</div>
		</div>

	</div>
	<!-- /.container -->

</div>
<!-- /.intro-header -->

<!-- Page Content -->

<a  name="services"></a>
<div class="content-section-a">

	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-sm-6">
				<hr class="section-heading-spacer">
				<div class="clearfix"></div>
				<h2 class="section-heading">Track your accomplishments on a daily basis</h2>
				<p class="lead">
					Now, you can produce detailed status reports easily.
					No more combing through past emails, code commits, etc. at the end of the week.
					By documenting a little bit each day, you'll be amazed at how much you're getting done each week!
				</p>
			</div>
			<div class="col-lg-5 col-lg-offset-2 col-sm-6">
				<img class="img-responsive" src="img/ipad.png" alt="">
			</div>
		</div>

	</div>
	<!-- /.container -->

</div>
<!-- /.content-section-a -->

<div class="content-section-b">

	<div class="container">

		<div class="row">
			<div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
				<hr class="section-heading-spacer">
				<div class="clearfix"></div>
				<h2 class="section-heading">Once a week, receive a summary of what you got done over the past week</h2>
				<p class="lead">
					Receive an elegantly formatted recap of your accomplishments from the previous week, ready to be
					presented to anyone, from clients to managers.
				</p>
			</div>
			<div class="col-lg-5 col-sm-pull-6  col-sm-6">
				<img class="img-responsive" src="img/dog.png" alt="">
			</div>
		</div>

	</div>
	<!-- /.container -->

</div>
<!-- /.content-section-b -->

<div class="content-section-a">

	<div class="container">

		<div class="row">
			<div class="col-lg-5 col-sm-6">
				<hr class="section-heading-spacer">
				<div class="clearfix"></div>
				<h2 class="section-heading">Share with your team, or just use it to track your own progress over time.</h2>
				<p class="lead">
					If you're working solo, seeing a steady stream of accomplishments can be highly rewarding, and motivate
					you to push towards the next milestone.
				</p>
			</div>
			<div class="col-lg-5 col-lg-offset-2 col-sm-6">
				<img class="img-responsive" src="img/phones.png" alt="">
			</div>
		</div>

	</div>
	<!-- /.container -->

</div>
<!-- /.content-section-a -->

<a  name="contact"></a>
<div class="banner">

	<div class="container">

		<div class="row">
			<div class="col-lg-6">
				{{--<h2>Connect to Start Bootstrap:</h2>--}}
			</div>
			<div class="col-lg-6">
				{{--<ul class="list-inline banner-social-buttons">--}}
					{{--<li>--}}
						{{--<a href="https://twitter.com/SBootstrap" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a>--}}
					{{--</li>--}}
					{{--<li>--}}
						{{--<a href="https://github.com/IronSummitMedia/startbootstrap" class="btn btn-default btn-lg"><i class="fa fa-github fa-fw"></i> <span class="network-name">Github</span></a>--}}
					{{--</li>--}}
					{{--<li>--}}
						{{--<a href="#" class="btn btn-default btn-lg"><i class="fa fa-linkedin fa-fw"></i> <span class="network-name">Linkedin</span></a>--}}
					{{--</li>--}}
				{{--</ul>--}}
			</div>
		</div>

	</div>
	<!-- /.container -->

</div>
<!-- /.banner -->

<!-- Footer -->
<footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="list-inline">
					<li>
						<a href="/">Home</a>
					</li>
					<li class="footer-menu-divider">&sdot;</li>
					<li>
						<a href="/auth/register">Signup</a>
					</li>
					<li class="footer-menu-divider">&sdot;</li>
					<li>
						<a href="/auth/login">Login</a>
					</li>
				</ul>
				<p class="copyright text-muted small">Copyright &copy; Status Knight 2016. All Rights Reserved</p>
			</div>
		</div>
	</div>
</footer>

<!-- jQuery -->
<script src="js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-72788840-1', 'auto');
	ga('send', 'pageview');

</script>

</body>

</html>
