	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/"><img src="/images/statusknight-logo.png" alt="Status Knight Logo"></a>
			</div>

			<div class="collapse navbar-collapse" id="navbar">
				<ul class="nav navbar-nav">
					@if(auth()->guest())
						{{--<li><a href="{{ url('/about') }}">About</a></li>--}}
						{{--<li><a href="{{ url('/foo') }}">Foo</a></li>--}}
					@else
						<li><a href="{{ url('/') }}">Home</a></li>
						<li><a href="{{ url('/user/report') }}">Report</a></li>
						@endif
				</ul>

				<ul class="nav navbar-nav navbar-right">
					@if(auth()->guest())
						<li><a href="{{ url('/auth/register') }}">Signup</a></li>
						<li><a href="{{ url('/auth/login') }}">Login</a></li>
					@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
								{{ auth()->user()->name }} <span class="caret"></span>
							</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/user/settings') }}">Settings</a></li>
								<li><a href="{{ url('/schedule') }}">Schedule</a></li>
								<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>

	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="alert alert-warning alert-dismissible hide" id="timezone-alert" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Your timezone has been detected as <span id="user-timezone"></span>.  To change this, click <a href="{{ url('/user/settings') }}" class="alert-link">here</a>.
			</div>
		</div>
	</div>



