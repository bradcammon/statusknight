@extends('layout')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Account</div>

				<div class="panel-body">
                                    
                                    {!! Form::open(array('action' => ['UserController@update', $user->id], 'method' => 'put')) !!}

                                    <div class="form-group">
                                        {!! Form::label('timezone', 'Timezone') !!}
                                        {!! Form::select('timezone', $tzList, $userTimeZoneKey, ['class' => 'form-control']) !!}
                                    </div>

                                    <div class='form-group'>
                                        {!! Form::submit('Submit', ['class' => 'btn btn-primary form-control']) !!}
                                    </div>

                                    {!! Form::close() !!}                                    
                                    
                                    
                                    


				</div>
			</div>
		</div>
	</div>
</div>
@endsection