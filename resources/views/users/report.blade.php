@extends('layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Schedule</div>
                    <div class="panel-body">

                        {!! Form::open(array('action' => ['UserController@getReport', $user->id], 'method' => 'get','class' => 'form-inline')) !!}

                            <div class='form-group'>
                                {!! Form::label('startDate', 'Start Date') !!}
                                {!! Form::text('startDate', $startDate , array('id' => 'startDate', 'class' => 'form-control') ) !!}

                                {!! Form::label('endDate', 'End Date') !!}
                                {!! Form::text('endDate', $endDate, array('id' => 'endDate', 'class' => 'form-control') ) !!}

                            </div>

                            <div class='form-group'>
                                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                            </div>

                            {!! Form::close() !!}


                            <div id="status-container">

                                <h3 class="page-heading">Your Status Items from {{ $startDate }} to {{ $endDate }}</h3>

                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <th>Status Item</th>
                                    {{--<th>Date</th>--}}
                                    </thead>

                                    <tbody>
                                    @foreach ($items as $item)
                                        <tr>
                                            <td> - {{ $item->raw_text }}</td>
                                            {{--<td>{{ $item->status_date }}</td>--}}
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

