@extends('layout')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Schedule</div>

				<div class="panel-body">

                    {!! Form::open(array('action' => ['ScheduleController@scheduleUpdate', $user->id], 'method' => 'put')) !!}

                    <div class="form-group">
                        {!! Form::label('timezone', 'Timezone') !!}
                        {!! Form::select('timezone', $tzList, $userTimeZoneKey, ['class' => 'form-control']) !!}
                    </div>

                    {{--<div class="form-group">--}}
                        {{--<label class="checkbox-inline">--}}
                            {{--{!! Form::checkbox('daily_email_optin', null, true ,['id' => 'daily-email-optin']) !!}--}}
                            {{--Send me daily reminders--}}
                        {{--</label>--}}
                    {{--</div>--}}

                    <fieldset id="daily-email">
                        <div class="form-group">
                            {!! Form::label('email_time', 'Daily Reminder Time') !!}
                            {!! Form::select('email_time', $hours, $userHoursKey, ['class' => 'form-control', 'id'=>'email_time']) !!}
                        </div>

                        <div class="form-group">
                            <label class="checkbox-inline">
                                {!! Form::checkbox('reminder_days[sunday]', null, $localWeekdays['sunday'] ) !!}
                                Sunday
                            </label>


                            <label class="checkbox-inline">
                                {!! Form::checkbox('reminder_days[monday]', null, $localWeekdays['monday']) !!}
                                Monday
                            </label>

                            <label class="checkbox-inline">
                            {!! Form::checkbox('reminder_days[tuesday]', null, $localWeekdays['tuesday']) !!}
                                Tuesday
                            </label>

                            <label class="checkbox-inline">
                            {!! Form::checkbox('reminder_days[wednesday]', null, $localWeekdays['wednesday']) !!}
                                Wednesday
                            </label>

                             <label class="checkbox-inline">
                            {!! Form::checkbox('reminder_days[thursday]', null, $localWeekdays['thursday']) !!}
                                 Thursday
                             </label>

                             <label class="checkbox-inline">
                            {!! Form::checkbox('reminder_days[friday]', null, $localWeekdays['friday']) !!}
                                 Friday
                             </label>

                             <label class="checkbox-inline">
                            {!! Form::checkbox('reminder_days[saturday]', null, $localWeekdays['saturday']) !!}
                                 Saturday
                             </label>
                        </div>
                    </fieldset>

                    <div class="form-group">
                        {!! Form::label('report_time', 'Weekly Report Time') !!}
                        {!! Form::select('report_time', $hours, $userReportHoursKey, ['class' => 'form-control', 'id'=>'report_time']) !!}
                    </div>

                    <div class="form-group">
                        <label class="radio-inline">
                            {!! Form::radio('report_day[]', 'sunday', $localReportWeekdays['sunday'] ) !!}
                            Sunday
                        </label>

                        <label class="radio-inline">
                            {!! Form::radio('report_day[]', 'monday', $localReportWeekdays['monday'] ) !!}
                            Monday
                        </label>

                        <label class="radio-inline">
                            {!! Form::radio('report_day[]', 'tuesday', $localReportWeekdays['tuesday'] ) !!}
                                Tuesday
                        </label>

                        <label class="radio-inline">
                            {!! Form::radio('report_day[]', 'wednesday', $localReportWeekdays['wednesday'] ) !!}
                            Wednesday
                        </label>

                        <label class="radio-inline">
                            {!! Form::radio('report_day[]', 'thursday', $localReportWeekdays['thursday'] ) !!}
                            Thursday
                        </label>

                        <label class="radio-inline">
                            {!! Form::radio('report_day[]', 'friday', $localReportWeekdays['friday'] ) !!}
                            Friday
                        </label>

                        <label class="radio-inline">
                            {!! Form::radio('report_day[]', 'saturday', $localReportWeekdays['saturday'] ) !!}
                            Saturday
                        </label>
                    </div>

                    <div class='form-group'>
                        {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                    </div>

                    {!! Form::close() !!}

                    @if ($errors->any())
                        <ul id='create_errors' class='alert alert-danger'>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

				</div>
			</div>
		</div>
	</div>
</div>
@endsection