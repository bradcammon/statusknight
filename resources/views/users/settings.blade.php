@extends('layout')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Settings</div>

				<div class="panel-body">
                                    
                    {!! Form::model($user, array('action' => ['UserController@update', $user->id], 'method' => 'put')) !!}

                        <div class="form-group">
                            {!! Form::label('display_name', 'Display Name') !!}
                            {!! Form::text('name', null, ['class' => 'form-control', 'id'=>'display_name']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('email', 'Email') !!}
                            {!! Form::text('email', null, ['class' => 'form-control', 'id'=>'email']) !!}
                        </div>

                        <div class='form-group'>
                            {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                        </div>

                    {!! Form::close() !!}

                    {!! Form::model($user, array('action' => ['UserController@updatePassword', $user->id], 'method' => 'put')) !!}

                    <div class="form-group">
                        {!! Form::label('old_password', 'Old Password') !!}
                        {!! Form::password('old_password', ['class'=>'form-control']) !!}
                        {!! Form::label('new_password', 'New Password') !!}
                        {!! Form::password('new_password', ['class'=>'form-control']) !!}
                        {!! Form::label('new_password_confirmation', 'New Password Confirmation') !!}
                        {!! Form::password('new_password_confirmation', ['class'=>'form-control']) !!}
                    </div>

                    <div class='form-group'>
                        {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                    </div>
                        @if ($errors->password->any())
                            <ul id='create_errors_pwd' class='alert alert-danger'>
                                @foreach ($errors->password->all() as $error_pwd)
                                    <li>{{ $error_pwd }}</li>
                                @endforeach

                            </ul>
                        @endif

                    {!! Form::close() !!}



                    {!! Form::model($team, array('action' => ['UserController@teamUpdate', $user->id], 'method' => 'put')) !!}

                        <div class="form-group">
                            {!! Form::label('team_name', 'Team Name') !!}
                            {!! Form::text('name', null, ['class' => 'form-control', 'id'=>'team_name']) !!}
                        </div>

                        <div class='form-group'>
                            {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                        </div>

                    {!! Form::close() !!}




                    @if ($errors->any())
                        <ul id='create_errors' class='alert alert-danger'>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

				</div>
			</div>
		</div>
	</div>
</div>
@endsection